# Future Handles

A rust library to complete futures via a remote handle.

```rust
async fn func() -> Option<u32> {
     let (future, handle) = unsync::create();

     func_with_callback(|| {
         handle.complete(1);
     });

     match future.await {
         // The callback was invoked and the result set via the handle.
         Ok(res) => Some(res),
         // The callback was never invoked, but the handle has been dropped.
         Err(_) => None
     }
}
```