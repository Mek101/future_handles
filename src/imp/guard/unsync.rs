use std::cell::{RefCell, RefMut};
use std::rc::{Rc, Weak};

use crate::imp::guard::{GuardResult, Guardian, WeakGuardian};

pub struct UnsyncGuardian<T: ?Sized> {
    inner: Rc<RefCell<T>>,
}

impl<T> Guardian<T> for UnsyncGuardian<T> {
    type Target<'a>
    = RefMut<'a, T> where T: 'a;
    type Weak = WeakUnsyncGuardian<T>;

    fn new(payload: T) -> Self {
        Self {
            inner: Rc::new(RefCell::new(payload)),
        }
    }

    fn get_guard(&self) -> GuardResult<RefMut<T>> {
        GuardResult::Guarded(self.inner.borrow_mut())
    }

    fn get_weak(&self) -> Self::Weak {
        WeakUnsyncGuardian {
            inner: Rc::downgrade(&self.inner),
        }
    }
}

pub struct WeakUnsyncGuardian<T> {
    inner: Weak<RefCell<T>>,
}

impl<T> Clone for WeakUnsyncGuardian<T> {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl<T> WeakGuardian<T> for WeakUnsyncGuardian<T> {
    type Guardian = UnsyncGuardian<T>;

    fn upgrade(&self) -> Option<Self::Guardian> {
        self.inner.upgrade().map(|rc| UnsyncGuardian { inner: rc })
    }
}
