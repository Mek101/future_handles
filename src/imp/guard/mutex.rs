use crate::imp::guard::{GuardResult, Guardian, WeakGuardian};
use std::sync::{Arc, Mutex, MutexGuard, Weak};

pub struct MutexGuardian<T: ?Sized> {
    inner: Arc<Mutex<T>>,
}

impl<T> Guardian<T> for MutexGuardian<T> {
    type Target<'a> = MutexGuard<'a, T> where T: 'a;
    type Weak = WeakMutexGuardian<T>;

    fn new(payload: T) -> Self {
        Self {
            inner: Arc::new(Mutex::new(payload)),
        }
    }

    fn get_guard(&self) -> GuardResult<MutexGuard<T>> {
        match self.inner.lock() {
            Ok(guard) => GuardResult::Guarded(guard),
            Err(_) => GuardResult::Poisoned,
        }
    }

    fn get_weak(&self) -> Self::Weak {
        WeakMutexGuardian {
            inner: Arc::downgrade(&self.inner),
        }
    }
}

pub struct WeakMutexGuardian<T: ?Sized> {
    inner: Weak<Mutex<T>>,
}

impl<T> Clone for WeakMutexGuardian<T> {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl<T> WeakGuardian<T> for WeakMutexGuardian<T> {
    type Guardian = MutexGuardian<T>;

    fn upgrade(&self) -> Option<Self::Guardian> {
        self.inner.upgrade().map(|arc| MutexGuardian { inner: arc })
    }
}
