use std::ops::DerefMut;

pub mod mutex;
#[cfg(feature = "spin")]
#[cfg_attr(docsrs, doc(cfg(feature = "spin")))]
pub mod spin;
pub mod unsync;

pub enum GuardResult<T> {
    Guarded(T),
    Poisoned,
}

impl<T> GuardResult<T> {
    pub fn unwrap(self) -> T {
        match self {
            GuardResult::Guarded(payload) => payload,
            GuardResult::Poisoned => panic!("Unwrapping poisoned guard"),
        }
    }
}

pub trait Guardian<T> {
    type Target<'a>: DerefMut<Target = T>
    where
        Self: 'a;
    type Weak: WeakGuardian<T, Guardian = Self>;

    fn new(payload: T) -> Self;

    fn get_guard<'a>(&'a self) -> GuardResult<Self::Target<'a>>;

    fn get_weak(&self) -> Self::Weak;
}

pub trait WeakGuardian<T>: Clone {
    type Guardian: Guardian<T>;

    fn upgrade(&self) -> Option<Self::Guardian>;
}
