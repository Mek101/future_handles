use spinning_top::lock_api::MutexGuard;
use spinning_top::{RawSpinlock, Spinlock};
use std::sync::{Arc, Weak};

use crate::imp::guard::{GuardResult, Guardian, WeakGuardian};

pub struct SpinGuardian<T: ?Sized> {
    inner: Arc<Spinlock<T>>,
}

impl<T> Guardian<T> for SpinGuardian<T> {
    type Target<'a>
    = MutexGuard<'a, RawSpinlock, T> where T: 'a;
    type Weak = WeakSpinGuardian<T>;

    fn new(payload: T) -> Self {
        Self {
            inner: Arc::new(Spinlock::new(payload)),
        }
    }

    fn get_guard(&self) -> GuardResult<MutexGuard<'_, RawSpinlock, T>> {
        GuardResult::Guarded(self.inner.lock())
    }

    fn get_weak(&self) -> Self::Weak {
        WeakSpinGuardian {
            inner: Arc::downgrade(&self.inner),
        }
    }
}

pub struct WeakSpinGuardian<T: ?Sized> {
    inner: Weak<Spinlock<T>>,
}

impl<T> Clone for WeakSpinGuardian<T> {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl<T> WeakGuardian<T> for WeakSpinGuardian<T> {
    type Guardian = SpinGuardian<T>;

    fn upgrade(&self) -> Option<Self::Guardian> {
        self.inner.upgrade().map(|arc| SpinGuardian { inner: arc })
    }
}
