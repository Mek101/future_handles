use crate::imp::guard::Guardian;
use crate::imp::r#struct::{CompletableFutureImpl, CompleteHandleImpl};
use crate::imp::state::State;
use crate::HandleResult;

pub fn create<T, G>() -> (CompletableFutureImpl<T, G>, CompleteHandleImpl<T, G::Weak>)
where
    G: Guardian<State<HandleResult<T>>>,
{
    let guardian: G = Guardian::new(State::new());
    let weak: G::Weak = guardian.get_weak();
    (
        CompletableFutureImpl::new(guardian),
        CompleteHandleImpl::new(weak),
    )
}
