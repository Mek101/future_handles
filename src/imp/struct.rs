use std::marker::PhantomData;
use std::task::{Context, Poll};

use crate::imp::guard::{GuardResult, Guardian, WeakGuardian};
use crate::imp::state::State;
use crate::{HandleError, HandleResult};

pub struct CompleteHandleImpl<T, W>
where
    W: WeakGuardian<State<HandleResult<T>>>,
{
    inner: W,
    phantom: PhantomData<T>,
}

impl<T, G, W> CompleteHandleImpl<T, W>
where
    G: Guardian<State<HandleResult<T>>>,
    W: WeakGuardian<State<HandleResult<T>>, Guardian = G>,
{
    fn try_set(&self, value: HandleResult<T>) -> HandleResult<()> {
        if let Some(guardian) = self.inner.upgrade() {
            let (res, maybe_waker) = match guardian.get_guard() {
                GuardResult::Guarded(mut guard) => guard.try_set_result(value),
                GuardResult::Poisoned => return HandleResult::Err(HandleError::Poisoned),
            };

            if let Some(waker) = maybe_waker {
                waker.wake();
            }
            if res {
                return HandleResult::Ok(());
            }
        }
        HandleResult::Err(HandleError::Closed)
    }

    pub fn new(weak_guardian: W) -> Self {
        Self {
            inner: weak_guardian,
            phantom: PhantomData::default(),
        }
    }

    pub fn complete(self, value: T) -> HandleResult<()> {
        self.try_set(Ok(value))
    }
}

impl<T, W> Drop for CompleteHandleImpl<T, W>
where
    W: WeakGuardian<State<HandleResult<T>>>,
{
    fn drop(&mut self) {
        let _ = self.try_set(Err(HandleError::DroppedBeforeComplete));
    }
}

impl<T, G, W> Clone for CompleteHandleImpl<T, W>
where
    G: Guardian<State<HandleResult<T>>>,
    W: WeakGuardian<State<HandleResult<T>>, Guardian = G>,
{
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
            phantom: Default::default(),
        }
    }

    fn clone_from(&mut self, source: &Self) {
        self.inner = source.inner.clone();
    }
}

pub struct CompletableFutureImpl<T, G>
where
    G: Guardian<State<HandleResult<T>>>,
{
    inner: G,
    phantom: PhantomData<T>,
}

impl<T, G> CompletableFutureImpl<T, G>
where
    G: Guardian<State<HandleResult<T>>>,
{
    pub(crate) fn new(guardian: G) -> Self {
        Self {
            inner: guardian,
            phantom: Default::default(),
        }
    }
}

impl<T, G> CompletableFutureImpl<T, G>
where
    G: Guardian<State<HandleResult<T>>>,
{
    pub fn fake_poll(&self, cx: &mut Context<'_>) -> Poll<HandleResult<T>> {
        let mut guard = self.inner.get_guard().unwrap();
        match guard.try_consume() {
            None => {
                guard.update_waker(cx.waker().clone());
                Poll::Pending
            }
            Some(res) => Poll::Ready(res),
        }
    }
}
