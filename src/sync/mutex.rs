//! A thread-safe [`CompletableFuture`] and [`CompleteHandle`] implementation using a platform
//! provided Mutex for synchronization.
//!
//! [`CompletableFuture`]: CompletableFuture
//! [`CompleteHandle`]: CompleteHandle
use crate::imp::func;
use crate::imp::guard::mutex::{MutexGuardian, WeakMutexGuardian};
use crate::imp::r#struct::{CompletableFutureImpl, CompleteHandleImpl};
use crate::imp::state::State;
use crate::HandleResult;
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};

/// Creates a new [`CompletableFuture`] and the associated [`CompleteHandle`].
///
/// ```rust
/// # use future_handles::sync::mutex;
/// # async fn deadlocks_before_result() -> u32 {
/// let (future, handle) = mutex::create();
///
/// func_with_callback(|res| {
///     handle.complete(res);
/// });
///
/// match future.await {
///     // The callback was invoked and the result set via the handle.
///     Ok(res) => res,
///     // The callback was never invoked, but the handle has been dropped.
///     Err(err) => panic!("Handle was dropped without setting a value or is poisoned")
/// }
/// # }
/// # fn func_with_callback<F>(func: F)
/// #    where F: FnOnce(u32) {
/// #    func(1);
/// # }
/// ```
///
/// # Danger!
///
/// Be careful to not await the future **before** setting the complete value or dropping the handle
/// while in the same async block, or you will cause a **deadlock**!
///
/// For a safer API, see [`scoped`].
///
/// ## Deadlock Examples
///
/// Setting the result:
/// ```rust
/// # use future_handles::sync::mutex;
/// async fn deadlocks_before_result() {
///     let (future, handle) = mutex::create();
///
///     // Start awaiting here...
///     future.await.unwrap();
///
///     // The result is set here, but we'll never be able to reach it!
///     handle.complete(1);
/// }
/// ```
///
/// Dropping the [`CompleteHandle`]. Be careful as this is more subtle, and **MAY** cause a deadlock
/// depending on your compiler's implementation, as Rust is under no obligation to drop the
/// [`CompleteHandle`] before you await the future, as it's lifetime extends until the end of the
/// block:
/// ```rust
/// # use future_handles::sync::mutex;
/// async fn may_deadlock_before_drop() {
///     let (future, handle) = mutex::create::<()>();
///     // The handle could be dropped immediately here and never deadlock.
///
///     // Start awaiting here...
///     future.await.unwrap();
///
///     // Or it could be dropped here, but we'll never reach it, deadlocking!
/// }
/// ```
///
/// Introducing an inverse dependency between the [`CompletableFuture`] and it's [`CompleteHandle`]
/// will always deadlock:
/// ```rust
/// # use future_handles::sync::mutex;
/// async fn may_deadlock_before_drop() {
///     let (future, handle) = mutex::create::<bool>();
///
///     // Making the completion depend on the result of the computation itself will cause a deadlock.
///     if future.await.unwrap() {
///         handle.complete(false);
///     }
/// }
/// ```
///
/// [`CompletableFuture`]: CompletableFuture
/// [`CompleteHandle`]: CompleteHandle
/// [`scoped`]: scoped
pub fn create<T>() -> (CompletableFuture<T>, CompleteHandle<T>) {
    let (f, h) = func::create();
    (CompletableFuture(f), CompleteHandle(h))
}

/// Creates a new [`CompletableFuture`] and the associated [`CompleteHandle`] inside a scope.
///
/// An alternative API to [`create`], that while being less ergonomic is harder to deadlock (see
/// [`create`] documentation).
///
/// ```rust
/// # use future_handles::sync::mutex;
/// # async fn func() -> u32 {
/// let future = mutex::scoped(|handle| {
///     func_with_callback(|res| {
///         handle.complete(res);
///     });
/// });
///
/// match future.await {
///     // The callback was invoked and the result set via the handle.
///     Ok(res) => res,
///     // The callback was never invoked, but the handle has been dropped.
///     Err(err) => panic!("Handle was dropped without setting a value or is poisoned")
/// }
/// # }
/// # fn func_with_callback<F>(func: F)
/// #    where F: FnOnce(u32) {
/// #    func(1);
/// # }
/// ```
///
/// [`create`]: create
/// [`CompletableFuture`]: CompletableFuture
/// [`CompleteHandle`]: CompleteHandle
pub fn scoped<T, F>(func: F) -> CompletableFuture<T>
where
    F: FnOnce(CompleteHandle<T>),
{
    let (fut, handle) = create();
    func(handle);
    fut
}

/// A thread-safe handle to complete the associated [`CompletableFuture`].
/// It can be safely dropped without setting a completion value.
///
/// ```rust
/// # use future_handles::sync::mutex::CompleteHandle;
/// fn func(complete_handle: CompleteHandle<u32>) {
///     if let Some(res) = func_that_may_fail() {
///         // Set the result.
///         complete_handle.complete(res);
///     }
///
///     // Or just drop the handle.
/// }
/// # fn func_that_may_fail() -> Option<u32> {
/// #    Some(1)
/// # }
/// ```
///
/// If cloned, the handles race to complete the future.
///
/// ```rust
/// # use futures;
/// # use future_handles::sync::mutex::CompleteHandle;
/// async fn func(complete_handle: CompleteHandle<u32>) {
///     let clone = complete_handle.clone();
///
///     let a = async { complete_handle.complete(1); };
///     let b = async { clone.complete(2); };
///
///     futures::join!(a, b); // The handles race to set the result.
/// }
/// ```
///
/// [`CompletableFuture`]: CompletableFuture
pub struct CompleteHandle<T>(CompleteHandleImpl<T, WeakMutexGuardian<State<HandleResult<T>>>>);

impl<T> CompleteHandle<T> {
    /// Complete the future consuming the handle.
    /// Returns an error if the handle could not complete the value.
    pub fn complete(self, value: T) -> HandleResult<()> {
        self.0.complete(value)
    }
}

impl<T> Clone for CompleteHandle<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }

    fn clone_from(&mut self, source: &Self) {
        self.0.clone_from(&source.0)
    }
}

/// A thread-safe future that can only be completed by the associated [`CompleteHandle`].
///
/// Since the [`CompleteHandle`] can be dropped without setting a completion value,
/// `CompletableFuture` always wraps the return value in a [`HandleResult`].
/// ```rust
/// # use futures::Future;
/// # use future_handles::HandleResult;
/// # async fn func(completable_future: impl Future<Output = HandleResult<()>>) { ///
/// match completable_future.await {
///     Ok(res) => res,
///     Err(err) => panic!("Handle was dropped without setting a value or is poisoned")
/// }
/// # }
/// ```
///
/// [`CompleteHandle`]: CompleteHandle
/// [`HandleResult`]: crate::HandleResult
pub struct CompletableFuture<T>(CompletableFutureImpl<T, MutexGuardian<State<HandleResult<T>>>>);

impl<T> Future for CompletableFuture<T> {
    /// The associated [`CompleteHandle`] may be dropped before setting a result, as such, it must
    /// be handled [`Future`] side by returning a [`HandleResult`].
    ///
    /// [`Future`]: core::future::Future
    type Output = HandleResult<T>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        self.0.fake_poll(cx)
    }
}

#[cfg(test)]
mod tests {
    use std::thread;

    use futures::executor::ThreadPool;
    use futures::task::SpawnExt;

    use super::super::test_utils;
    use super::*;

    fn get_test_objects() -> (ThreadPool, CompletableFuture<()>, CompleteHandle<()>) {
        let (fut, handle) = create();
        (ThreadPool::new().unwrap(), fut, handle)
    }

    #[test]
    fn return_value() {
        let (pool, fut, handle) = get_test_objects();
        test_utils::return_value(pool, fut, (), |val| handle.complete(val));
    }

    #[test]
    fn drop() {
        let (pool, fut, handle) = get_test_objects();
        test_utils::drop(pool, fut, |val| handle.complete(val));
    }

    #[test]
    fn complete_before_await() {
        let (pool, fut, handle) = get_test_objects();
        test_utils::complete_before_await(pool, fut, (), |val| handle.complete(val));
    }

    #[test]
    fn race_complete() {
        let (pool, fut, handle) = get_test_objects();

        let handle_clone = handle.clone();

        pool.spawn(async move {
            fut.await.unwrap();
        })
        .unwrap();

        pool.spawn(async move {
            thread::yield_now();
            test_utils::check_ok_or_closed(handle.complete(()));
        })
        .unwrap();
        pool.spawn(async move {
            thread::yield_now();
            test_utils::check_ok_or_closed(handle_clone.complete(()));
        })
        .unwrap();
    }

    #[test]
    fn double_complete() {
        let (pool, fut, handle) = get_test_objects();

        let handle_clone = handle.clone();

        pool.spawn(async move {
            fut.await.unwrap();
        })
        .unwrap();

        pool.spawn(async move {
            thread::yield_now();
            handle.complete(()).unwrap();
            test_utils::check_closed(handle_clone.complete(()).unwrap_err());
        })
        .unwrap();
    }
}
