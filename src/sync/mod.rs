//! Thread-safe `CompletableFuture` and `CompleteHandle`] implementations.
pub mod mutex;
#[cfg(feature = "spin")]
#[cfg_attr(docsrs, doc(cfg(feature = "spin")))]
pub mod spin;
#[cfg(test)]
mod test_utils;
