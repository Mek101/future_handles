use std::future::Future;
use std::mem;

use futures::executor::ThreadPool;
use futures::task::SpawnExt;

use crate::{HandleError, HandleResult};

pub(crate) fn check_closed(err: HandleError) {
    match err {
        HandleError::DroppedBeforeComplete => {
            panic!("Complete handle has been dropped without completing")
        }
        HandleError::Closed => {}
        HandleError::Poisoned => panic!("Complete handle poisoned"),
    }
}

pub(crate) fn check_ok_or_closed<T>(res: HandleResult<T>) {
    match res {
        Ok(_) => {}
        Err(err) => check_closed(err),
    }
}

pub(crate) fn return_value<T: Send + 'static, F: Send + 'static, C: Send + 'static>(
    pool: ThreadPool,
    fut: F,
    val: T,
    func: C,
) where
    F: Future<Output = HandleResult<T>> + Send,
    C: FnOnce(T) -> HandleResult<T>,
{
    pool.spawn(async move {
        fut.await.unwrap();
    })
    .unwrap();

    pool.spawn(async {
        func(val).unwrap();
    })
    .unwrap();
}

pub(crate) fn drop<T: Send + 'static, F: Send + 'static, C: Send + 'static>(
    pool: ThreadPool,
    fut: F,
    func: C,
) where
    F: Future<Output = HandleResult<T>> + Send,
    C: FnOnce(T) -> HandleResult<T>,
{
    pool.spawn(async move {
        match fut.await {
            Err(err) => {
                match err {
                    HandleError::DroppedBeforeComplete => {} // Ok
                    HandleError::Closed => {
                        panic!("Complete handle dropped but future returned Closed")
                    }
                    HandleError::Poisoned => {
                        panic!("Complete handle dropped but future returned Poisoned")
                    }
                }
            }
            Ok(_) => panic!("Completer dropped but future returned Ok"),
        }
    })
    .unwrap();

    pool.spawn(async {
        mem::drop(func);
    })
    .unwrap();
}

pub(crate) fn complete_before_await<T: Send + 'static, F: Send + 'static, C: Send + 'static>(
    pool: ThreadPool,
    fut: F,
    val: T,
    func: C,
) where
    F: Future<Output = HandleResult<T>> + Send,
    C: FnOnce(T) -> HandleResult<T>,
{
    func(val).unwrap();
    pool.spawn(async move {
        fut.await.unwrap();
    })
    .unwrap();
}
